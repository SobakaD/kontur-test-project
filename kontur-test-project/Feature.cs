﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kontur_test_project
{
    class Feature<T1, T2> : IFeature<T1>, IFeature2<T2>
    {
        public string DoStuff(T1 stuff)
        {
            return $"Stuff({stuff.ToString()}) is done!";
        }

        public T2 MakeStuff(int something)
        {
            throw new NotImplementedException();
        }
    }
}
