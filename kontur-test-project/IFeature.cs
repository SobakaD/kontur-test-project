﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kontur_test_project
{
    interface IFeature<in T>
    {
        string DoStuff(T stuff);
    }
}
